package string.generators;

public class PersonSteven {

    private String name;
    private String firstName;

    public PersonSteven(String name, String firstName) {
        this.name = name;
        this.firstName = firstName;
    }

    public PersonSteven() {
        this("De Cock","Steven");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
