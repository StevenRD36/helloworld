package string.generators;


import java.util.concurrent.ThreadLocalRandom;

public class RandomGenerator {
    private RandomGenerator() {
    }

    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String NUMBERS = "0123456789";
    private static final int ALPHABET_LENGTH = 52;
    private static final String EMAIL_SUFFIX = "@realdolmen.com";

    private static String[] firstNames = {"Maxime", "Jeroen", "Yoni", "Timmy"};
    private static String[] lastNames = {"Esnol", "Buelens", "Vindjelinks", "Van Coekelaekenberhg"};


    public static int generateRandomint(int number) {
        return ThreadLocalRandom.current().nextInt(0, number);
    }

    public static String getRandomFirstName() {
        return firstNames[generateRandomint(firstNames.length)];
    }

    public static String getRandomLastName() {
        return lastNames[generateRandomint(lastNames.length)];
    }

    public static String[] getFirstNames() {
        return firstNames;
    }

    public static String[] getLastNames() {
        return lastNames;
    }

    public static String name() {
        return
                getRandomFirstName() +
                        " " +
                        getRandomLastName();
    }

    //generates an email
    public static String email(String firstName, String lastName) throws EmailMissingNameException {
        if (firstName.isEmpty() || lastName.isEmpty()) {
            throw new EmailMissingNameException();
        } else {
            return firstName + "." + lastName + EMAIL_SUFFIX;
        }
    }

    public static String password(int length) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < length; i++) {
            if (generateRandomint(2) < 1) {
                builder.append(NUMBERS.charAt(generateRandomint(10)));
            } else {
                builder.append(ALPHABET.charAt(generateRandomint(ALPHABET_LENGTH)));
            }
        }
        return builder.toString();
    }
}
